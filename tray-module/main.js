const electron = require('electron');
const url = require('url');
const path = require('path');

const {app, BrowserWindow, Tray, Menu} = electron;
const iconPath = path.join(__dirname, 'icon.png');
let tray = null;

app.on('ready', appReady);

function appReady(){
    //Creates an icon in the system tray
    tray = new Tray(iconPath);

    let template = [
        {
            label: 'Audio',
            submenu: [
                {
                    label: 'Low',
                    type: 'radio',
                    checked: true
                },
                {
                    label: 'High',
                    type: 'radio'
                }
            ]
        },
        {
            type: 'separator'
        },
        {
            label: 'Video',
            submenu: [
                {
                    label: '1280x720',
                    type: 'radio',
                    checked: true
                },
                {
                    label: '1920x1080',
                    type: 'radio'
                }
            ]
        }
    ]

    const ctxMenu = Menu.buildFromTemplate(template);
    tray.setContextMenu(ctxMenu);
    //To set a tooltip description to the system tray icon
    tray.setToolTip('Electron Tray Application');
    tray.on('click', ()=>{
        //Do something on left click
    });
}