const electron = require('electron');
const url = require('url');
const path = require('path');
const Menu = electron.Menu;
const MenuItem = electron.MenuItem;
const globalShortcut = electron.globalShortcut; 

const {
    app,
    BrowserWindow
} = electron;

let mainWindow;

app.on('ready', appReady);

function appReady() {
    mainWindow = new BrowserWindow();
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));
    createMenu();
    mainWindow.on('closed', () => {
        mainWindow = null;
    });
    app.on('will-quit', ()=>{
        globalShortcut.unregisterAll();
    });
    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });
    mainWindow.webContents.openDevTools();
}

function createMenu() {
    /**
     * Accelerators are keyboard shortcuts
     * If we use roles we need not specify keyboard shortcuts
     */
    const template = [{
            label: "Demo",
            submenu: [{
                    label: "Submenu 1",
                    click: () => {
                        console.log('Clicked submenu 1');
                    },
                    accelerator: "CmdOrCtrl + Shift + H"
                },
                {
                    type: 'separator'
                },
                {
                    label: "Submenu 2"
                }
            ]
        },
        {
            label: 'Edit',
            submenu: [
                {role: 'undo'},
                {role: 'paste'},
                
            ]
        },
        {
            label: 'Help',
            click: () => {
                electron.shell.openExternal('http://electron.atom.io');
            }
        }
    ]

    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);

    //CONTEXT MENU
    //after app-menu code
    const ctxMenu = new Menu();
    ctxMenu.append(new MenuItem({
        label: 'Hello',
        click: () => {
            console.log('Context menu item HELLO was clicked');
        }
    }));
    ctxMenu.append(new MenuItem({
        label: 'Next',
        click: () => {
            console.log('Context menu item NEXT was clicked');
        },
        accelerator: 'CmdOrCtrl + Shift + H'
    }));
    mainWindow.webContents.on('context-menu', function (event, params) {
        //popup() opens context menu. params.x and params.y gives coordinates where the right click occured.
        ctxMenu.popup(mainWindow, params.x, params.y);
    });

    //Global shortcuts will work even when app is not in focus. They will be unregistered when app quits
    globalShortcut.register('Alt + 1', function(){
        mainWindow.show();
    });
}