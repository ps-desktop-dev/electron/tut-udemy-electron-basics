const electron = require('electron');
const url = require('url');
const path = require('path');

const {app,BrowserWindow} = electron;

let mainWindow, dimWindow, colorWindow, framelessWindow;

app.on('ready', appReady);

function appReady(){
    mainWindow = new BrowserWindow();
    /**
     * maxHeight and maxWidth are the max bounds to which the window can be stretched
     * title is the title of the window
     * modal property if true will not allow focusing parent until the child is closed
     * parent property specifies the parent window object
     * show property if false will show the window only after loading the URL
     */
    dimWindow = new BrowserWindow({
        show:false,
        width: 300,
        height: 300,
        maxHeight: 400,
        maxWidth: 600,
        parent: mainWindow,
        title: 'Child',
        modal: true
    });
    dimWindow.loadURL('https://google.co.in');
    dimWindow.once('ready-to-show', ()=>{
        dimWindow.show();
    });
    colorWindow = new BrowserWindow({
        backgroundColor: '#228d22'
    });
    framelessWindow = new BrowserWindow({
        frame: false,
        backgroundColor: '#FFF555'
    });
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'main.html'),
        protocol: 'file:',
        slashes: true
    }));
    mainWindow.on('closed', ()=>{
        mainWindow = null;
    });
    app.on('window-all-closed', ()=>{
        if(process.platform !== 'darwin'){
            app.quit();
        }
    })
    mainWindow.webContents.openDevTools();
}