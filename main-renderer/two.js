console.log('From two.js')
const BrowserWindow = require('electron').remote.BrowserWindow;
const url = require('url');
const path = require('path');

let win4;
var head = document.getElementById('head');
head.addEventListener('click', (event)=>{
    win4 = new BrowserWindow();
    win4.loadURL(url.format({
        pathname: path.join(__dirname, 'index4.html'),
        protocol: 'file:',
        slashes: true
    }));
});