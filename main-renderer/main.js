console.log('Form main.js')
const electron = require('electron');
const url = require('url');
const path = require('path');

const {app,BrowserWindow} = electron;

let mainWindow, win2;

app.on('ready', appReady);

function appReady(){
    mainWindow = new BrowserWindow();
    win2 = new BrowserWindow();
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));
    win2.loadURL(url.format({
        pathname: path.join(__dirname, 'index2.html'),
        protocol: 'file:',
        slashes: true
    }));
    mainWindow.on('closed', ()=>{
        mainWindow = null;
    });
    win2.on('closed', ()=>{
        win2 = null;
    });
    app.on('window-all-closed', ()=>{
        if(process.platform !== 'darwin'){
            app.quit();
        }
    })
    mainWindow.webContents.openDevTools();
}