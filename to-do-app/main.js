const electron = require('electron');
const url = require('url');
const path = require('path');

const {
    app,
    BrowserWindow,
    Menu,
    ipcMain
} = electron;

let mainWindow, addWindow;

app.on('ready', appReady);

function appReady() {
    mainWindow = new BrowserWindow();
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));
    mainWindow.on('closed', () => {
        mainWindow = null;
        app.quit();
    });
    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    })
    mainWindow.webContents.openDevTools();

    if(process.platform === 'darwin'){
        menuTemplate.unshift({});
    }
    const mainMenu = Menu.buildFromTemplate(menuTemplate);
    Menu.setApplicationMenu(mainMenu);
}

let menuTemplate = [
    {
        label: 'Action',
        submenu: [
            {
                label: 'Add todo',
                click(){
                    createAddWindow();
                }
            }, 
            {
                label: 'Clear todos',
                click(){
                    mainWindow.webContents.send('todo:clear');
                }
            },
            {
                label: 'Quit',
                click(){
                    app.quit();
                },
                accelerator: 'CmdOrCtrl + Q'
            }
        ]
    }
];

if(process.env.NODE_ENV !== 'production'){
    menuTemplate.push({
        label: 'Developer',
        submenu: [
            {
                role: 'reload'
            },
            {
                label: 'Toggle Developer Tools',
                click(item, focusedWindow){
                    focusedWindow.toggleDevTools();
                }
            }
        ]
    });
}

function createAddWindow(){
    addWindow = new BrowserWindow({
        width: 300,
        height: 200,
        title: 'Add Todo',
        parent: mainWindow       
    });
    addWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'add.html'),
        slashes: true,
        protocol: 'file:'
    }));
    addWindow.webContents.openDevTools();
    addWindow.on('closed', ()=>{
        addWindow = null;
    });
}
ipcMain.on('todo:add', (event, todo)=>{
    mainWindow.webContents.send('todo:add', todo);
    addWindow.close();
});
