const electron =require('electron');
const ipc = electron.ipcRenderer;
/**
 * IPC is of 2 types
 * Async IPC : Do not lock other operations
 * Sync IPC : Locks other operations
 */
const asyncErrBtn = document.getElementById('asyncBtn');
asyncErrBtn.addEventListener('click', function(){
    console.log('Async Message 1');
    ipc.send('async-message');
    console.log('Async Message 2'); //This will get executed before reply for the IPC send is received
});

ipc.on('async-reply', (event, arg)=>{
    console.log(arg);
});

const syncErrBtn = document.getElementById('syncBtn');
syncErrBtn.addEventListener('click', function(){
    console.log('Sync Message 1');
    const reply = ipc.sendSync('sync-message');
    console.log(reply);
    console.log('Sync Message 2'); //This will not execute until a reply is got for the IPC send
});

/**
 * Remote module
 * To invoke native apis IPC is required b/w main and renderer process.
 * Remote module can be used to invoke methods of main process without explicitely sending IPC message
 */
const BrowserWindow = electron.remote.BrowserWindow;
//BrowserWindow is from the main process accessed remotely. Remote module uses SYNC IPC
let window = new BrowserWindow();
window.loadURL('https://www.facebook.com');