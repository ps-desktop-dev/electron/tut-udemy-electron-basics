const electron = require('electron');
const url = require('url');
const path = require('path');

const {
    app,
    BrowserWindow
} = electron;
const ipc = electron.ipcMain;
const dialog = electron.dialog;

let mainWindow;

app.on('ready', appReady);

function appReady() {
    mainWindow = new BrowserWindow();
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));
    mainWindow.on('closed', () => {
        mainWindow = null;
    });
    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });
    ipc.on('async-message', (event) => {
        /**
         * to show error dialog
         * dialog.showErrorBox('Error Message', 'Demo of error message');
         */
        event.sender.send('async-reply', 'Message to renderer: error dialog was displayed');
    });
    ipc.on('sync-message', (event) => {
        /**
         * to show error dialog
         * dialog.showErrorBox('Error Message', 'Demo of error message');
         */
        event.returnValue = 'sync-reply';
    });
    mainWindow.webContents.openDevTools();
}