//To work with files use file system module in node
const fs = require('fs');
const path = require('path');

let btnCreate, btnRead, btnDelete, fileName, fileContents;

btnCreate = document.getElementById('btnCreate');
btnRead = document.getElementById('btnRead');
btnDelete = document.getElementById('btnDelete');
fileName = document.getElementById('fileName');
fileContents = document.getElementById('fileContents');

//Creating a folder in our project for CRUD operations
let pathName = path.join(__dirname, 'Files');
btnCreate.addEventListener('click', function(){
    let file = path.join(pathName, fileName.value);
    let contents = fileContents.value;
    fs.writeFile(file, contents, function(err){
        if(err){
            return console.error(err);
        }
        console.log('File was created');
    });
});

btnRead.addEventListener('click', function(){
    let file = path.join(pathName, fileName.value);
    fs.readFile(file, function(err, data){
        if(err){
            return console.error(err);
        }
        fileContents.value = data;
        console.log("File was read");
    });
});

btnDelete.addEventListener('click', function(){
    let file = path.join(pathName, fileName.value);
    fs.unlink(file, function(err){
        if(err){
            return console.error(err);
        }
        fileName.value = '';
        fileContents.value = '';
        console.log("File was deleted");
    });
});